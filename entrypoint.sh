#!/bin/sh

[ ! -d "/input" ] && sleep 600;

while true; do
	for input_file in /input/*.mp4; do
		ffmpeg -re -i "${input_file}" -c copy -f flv "rtmps://dc4-1.rtmp.t.me/s/${STREAM_KEY}";
		sleep 3;
	done;
done
